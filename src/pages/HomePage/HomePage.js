import React from 'react'
import {
  Example,
} from 'components'
import {
  UserListContainer,
} from 'containers'
import {
  ROUTE_PATH,
  redirect,
} from 'helpers'

export class HomePage extends React.Component {
  render() {
    return (
      <React.Fragment>
        <div>{ROUTE_PATH.HOME.TEXT}</div>
        <Example>
          <Example.Children>Children</Example.Children>
        </Example>
        <UserListContainer/>
        <button
          onClick={() => {
            redirect(ROUTE_PATH.ABOUT.LINK)
          }}
        >
          Go to {ROUTE_PATH.ABOUT.TEXT}
        </button>
      </React.Fragment>
    )
  }
}
