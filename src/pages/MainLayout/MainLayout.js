import React from 'react'

export class MainLayout extends React.Component {
  render() {
    const {
      children,
    } = this.props

    return (
      <React.Fragment>
        <div>Main Container</div>
        {children}
      </React.Fragment>
    )
  }
}
