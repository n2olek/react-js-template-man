import React from 'react'
import {
  Route
} from 'react-router-dom'
import {
  connect,
} from 'react-redux'
import {
  setRouteHistory,
} from 'actions'
import {
  MainLayout,
  HomePage,
  AboutPage,
} from 'pages'
import {
  ROUTE_PATH,
} from 'helpers'

export class RouteApp extends React.Component {

  constructor(props) {
    super(props)
    props.setRouteHistory(props.history)
    this.state = {}
  }

  render() {
    return (
      <MainLayout>
        {/* Use props 'exact' for match single container(not share container) */}
        <Route exact path={ROUTE_PATH.HOME.LINK} component={HomePage} />
        <Route exact path={ROUTE_PATH.ABOUT.LINK} component={AboutPage} />
      </MainLayout>
    )
  }
}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setRouteHistory: (data) => dispatch(setRouteHistory(data))
  }
}

export const RouteContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RouteApp)